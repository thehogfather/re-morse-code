const assert = require("assert");
const { encodeLine, decodeLine, encodeMultiLine, decodeMultiLine } = require("../src/ReMorseCode");

const word = "HELLO",
      letter = "E",
      blank = "",
      line = "HELLO THERE",
      zero = "0",
      thousand = "1,000",
      numericLine = "ONE THOUSAND 1,000 1.5K",
      multiLine = `HELLO
I AM IN TROUBLE AND REMORSEFUL`,
      multiLineWithEmptyLines = `HELLO

I AM IN TROUBLE`;

const tests = [word, letter, blank, line, zero, thousand, numericLine];
const testsMultiLine = [multiLine, multiLineWithEmptyLines];
const testErrors = ["4?f"];

const runTests = () => {
    describe("ReMorseCode encodings", function () {
        it("should return the plain text when running decode(encode(plain_text)) for a single line", function () {
            tests.forEach(test => assert.deepEqual(decodeLine(encodeLine(test)), test));
        });
        
        it("should return the plain text when running decode(encode(plain_text)) for multiple lines", function () {
            testsMultiLine.forEach(test => assert.deepEqual(decodeMultiLine(encodeMultiLine(test)), test));
        });
        
        it("should throw an error when encoding plain text with unrecognised characters", function () {
            testErrors.forEach(test => {
                assert.throws(() => encodeLine(test), Error);
            });        
        });
    });
};

runTests();
