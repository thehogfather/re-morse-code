const letters = require("./letters.json");
const letterToMorse = require("./letterToMorse.json");
const morseToLetter = require("./morseToLetter.json");
const { readUntil, repeat } = require("./utils");
const os = require("os");

/**
    Encodes a single morse character representation, into the equivalent `remorse` code.
    @param {string} morseCode The morse code to convert.
    @return {string} The `remorse` code encoding of the morse code supplied.
*/
const morseToRemorse = (morseCode) => {
    let i = 0;
    let result = "";
    while (i < morseCode.length) {
        if (morseCode[i] === ".") {
            let numberOfDots = readUntil(morseCode, i, character => character !== ".");
            i = numberOfDots.end;
            result = result + numberOfDots.count;
        } else if (morseCode[i] === "-") {
            let numberOfDashes = readUntil(morseCode, i, character => character !== "-");
            i = numberOfDashes.end;
            result = result + letters[numberOfDashes.count - 1];
        } else {
            throw Error(`unexpected character ${morseCode[i]} in morse code.`);
        }
    }
    
    return result;
};

/**
    Converts a single morse word to the equivalent representation in `remorse`.
    @param {string} word The morse word to convert.
    @return {string} The `remorse` code equivalent of the word supplied
*/
const morseWordToRemorse = (word) => {
    return word.split("|")
        .map(morseToRemorse)
        .join("|");
};

/**
    Converts a line of morse code to the equivalent representation in `remorse`.
    @param {string} line The line of morse code to convert.
    @return {string} The `remorse` code equivalent of the line supplied.
*/
const morseLineToRemorse = (line) => {
    return line.split("/")
        .map(morseWordToRemorse)
        .join("/");
};

/**
    Decodes a morse word to plain text
    @param {string} word A morse encoded word with `|` as character separator
    @return {string} Plain text decoding of the morse code
*/
const morseWordToText = (word) => {
    return word.split("|")
        .map(character => morseToLetter[character])
        .join("");
};

/**
    Decodes encoded `remorse` code to morse code.
    @param {string} word the encoded word to convert
    @return {string} The morse code representation of the encoded string
*/
const remorseWordToMorse = (word) => {
    const toMorse = (character) => {
        if (Number.isNaN(parseInt(character))) {
            let index = letters.indexOf(character);
            return repeat("-", index + 1);
        } else {
            return repeat(".", parseInt(character));
        }
    };
    
    return word.split("|")
        .map(morseChunk => morseChunk.split("").map(toMorse).join(""))
        .join("|")
};

/**
    Utility function to covert a line of `remorse` code to a line of morse code.
    @param {string} line A line of encoded morse code
    @return {string} A line of morse code
*/
const remorseLineToMorse = (line) => {
    return line.split("/")
        .map(remorseWordToMorse)
        .join("/");
}

/**
    Converts a line of morse code to plain text
    @line {string} line The line of morse code to convert
    @return {string} The decoded plain text version of the morse code
*/
const morseLineToPlainText = (line) => {
    return line.split("/")
        .map(morseWordToText)
        .join(" ");
}

/**
    Converts a plain text word to morse code word.
    @param {string} text Plain text to convert
    @return {string} A word of morse code
*/
const plainWordToMorse = (text) => {
    return text.split("")
        .map(char => char.toUpperCase())
        .map(char => {
            if (letterToMorse[char]) {
                return letterToMorse[char];
            } else {
                throw Error(`Unknown mapping from plain text to morse character "${char}"`);
            }
        }).join("|");
};

/**
    Utility function to convert a line of plain text to morse code.
    @param {string} line The line of plain text to convert
    @return {string} A line of morse code
*/
const plainLineToMorse = (line) => {
    return line.split(/\s+/)
        .map(plainWordToMorse)
        .join("/")
};

/**
    Converts a line of plain text to `remorse` code.
    @param {string} line The line of plain text to convert
    @return {string} The encoded line of text.
*/
const encodeLine = (line) => {
    let morse = plainLineToMorse(line);
    return morseLineToRemorse(morse);
};

/**
    This is currently used in the tests to check that encode is the inverse of decode.
    Converts `remorse` code back into plain text.
    @param {string} remorseLine A line of `remorse` code to convert to plain text.
    @return {string} The plain text version of the encoded string
*/
const decodeLine = (remorseLine) => {
    let morse = remorseLineToMorse(remorseLine);
    return morseLineToPlainText(morse);
};

// API for the ReMorseCode module
module.exports = {
    encodeLine,
    decodeLine,
    encodeMultiLine(text) {
        return text.split(os.EOL)
            .map(encodeLine)
            .join(os.EOL);
    },
    decodeMultiLine(text) {
        return text.split(os.EOL)
            .map(decodeLine)
            .join(os.EOL);
    }
};