/**
    Utility function to read a string from the specified index until the
    given predicate is true.
    @param {string} string The string to read
    @param {number} start The index to start from.
    @param {(char) => boolean} predicate A test function to signal when to stop the read operation
    @return {count: number, end: number} The number of items read and the first index where the predicate failed.
*/
const readUntil = (string, start, predicate) => {
    let i = start;
    for (i = start; i < string.length; i++) {
        if (predicate(string[i])) {
            return {count: (i - start), end: i};
        }
    }
    return {count: (i - start), end: i};
};

/**
    Repeats a specified character `count` number of times.
    @param {string} character The string/character to repeat
    @param {number} count The number of times to repeat the character
    @return {string} A string repeating `character` `count` times
*/
const repeat = (character, count) => {
    if (Number.isNaN(parseInt(count)) || count === null) {
        return character;
    }
    let result = [], i = 0;
    while (i < count) {
        result.push(character);
        i++;
    }
    return result.join("");
};

module.exports = {
    readUntil,
    repeat
};