const { encodeMultiLine } = require("./ReMorseCode");
const fs = require("fs");
const os = require("os");
const path = require("path");

let [, , filename] = process.argv;
if (!filename || typeof filename !== "string") {
    filename = path.resolve(__dirname, "../test/test.txt");
}

require.extensions[".txt"] = (module, filename) => {
    module.exports = fs.readFileSync(filename, "utf8");
}

const run = () => {
    const txt = require(filename);
    const encodedText = encodeMultiLine(txt);

    const baseName = path.basename(filename, path.extname(filename));
    const baseDir = path.dirname(filename);
    const outputFileName = `${baseDir}/${baseName}.enc.txt`;
    fs.writeFile(outputFileName, encodedText, (error) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`Done! Secret file written to ${outputFileName}`);
        }
    });
};

run();


